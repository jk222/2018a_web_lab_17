package ictgradschool.ictgradschool.web.lab17.ex01;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Servlet implementation class CGIParams
 */
public class CGIParams extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CGIParams() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		
		//displayBasic(request, response);
		
		displayHTML(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
	
	private void displayBasic(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// http://stackoverflow.com/questions/1928675/servletrequest-getparametermap-returns-mapstring-string-and-servletreques
		
		// https://docs.oracle.com/javaee/6/api/javax/servlet/ServletRequest.html
		
		Map<String, String[]> map = request.getParameterMap();
		Iterator<Entry<String, String[]>> i = map.entrySet().iterator();
		response.getWriter().println("\n\nkey: values");

		while(i.hasNext()) {
			Entry<String, String[]> entry = i.next();
			String key = entry.getKey();
			String[] values = entry.getValue();
			response.getWriter().print("\n" + key.toUpperCase() + ": ");
			for(String value: values) {
				response.getWriter().print(value + ",");
			}
		}
		response.getWriter().print("\n");
	} 

	private void displayHTML(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			// http://stackoverflow.com/questions/1928675/servletrequest-getparametermap-returns-mapstring-string-and-servletreques
		
			// https://docs.oracle.com/javaee/6/api/javax/servlet/ServletRequest.html
			
		// "/" is relative to the context root (your web-app name)
		// don't add your web-app name to the path
		
		//RequestDispatcher view = request.getRequestDispatcher("/response.html"); // works, but /response.jsp causes 500 "jsp support not configured"
        //view.forward(request, response);  



		
		PrintWriter out = response.getWriter();
		out.println("<html>\n<head><title>Server response</title>");
		out.println("</head>\n<body>");
		out.println("<h1>Server side response</h1>");
		out.println("<p>Thanks for your submission. The values sent to the server are as follows:</p>");
		
		
		Map<String, String[]> map = request.getParameterMap();
		Iterator<Entry<String, String[]>> i = map.entrySet().iterator();


		        //USED when specific parameters are required to be displayed.

		out.println("<table><tr><th>Name:</th><th>"+request.getParameter("name")+"</th></tr>");
        out.println("<tr><th>Email:</th><th>"+request.getParameter("email")+"</th></tr>");
        out.println("<tr><th>Your city:</th><th>"+request.getParameter("city")+"</th></tr>");
        out.println("<tr><th>Course choice:</th><th>"+request.getParameter("course_choice")+"</th></tr>");
		out.println("<tr><th> Your experience:</th><th>"+request.getParameter("Experience")+"</th></tr>");
		out.println("<tr><th> Favourite Animal:</th><th>"+request.getParameter("animalName")+"</th></tr>");
		out.println("<tr><th> Text Content</th><th>"+request.getParameter("profile_text_area")+"</th></tr>");
		out.println("</table>");
        out.println("<br>");
                //USED when all parameters are required to be displayed.

        out.println("<table>");
		while(i.hasNext()) {
			out.println("<tr>");
			Entry<String, String[]> entry = i.next();
			String key = entry.getKey(); //.toUpperCase();
			String[] values = entry.getValue();
			if(key.contains("submit") || key.contains("button")) {
				continue; 
			}
			int index = key.indexOf("[]");
			if(index != -1) {
				key = key.substring(0, index);
			}
			out.print("<td>" + key + "</td><td>");
			for(String value: values) {
				out.print("&lt;" + value + "&gt; ");
			}
			out.println("</td></tr>");
		}


		out.println("</table>");
		out.println("</body></html>");
	}
}
