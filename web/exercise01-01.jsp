<%@ page import="javax.swing.text.Document" %>
<%@ page import="java.time.LocalDateTime" %>
<%@ page import="java.time.Clock" %><%--
  Created by IntelliJ IDEA.
  User: jk222
  Date: 15/05/2018
  Time: 11:43 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>Text Example 1</title>
    </head>
    <body>

        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce et dolor id velit eleifend maximus. Quisque eget sem non magna commodo aliquam non sit amet massa. Aliquam mollis risus id quam efficitur, vitae eleifend libero volutpat. Cras sagittis maximus massa, at iaculis nisl sagittis a. Nam molestie enim at metus consectetur, sit amet interdum orci tincidunt. Nullam volutpat leo sed diam pellentesque, auctor commodo velit blandit. Vivamus fermentum non ante ac semper. Fusce tincidunt scelerisque nisi at vestibulum. Duis dictum nunc finibus, ultrices nulla sed, dictum mauris. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>

        <p>Etiam posuere ut augue ac ultrices. Suspendisse vitae metus laoreet, auctor velit at, bibendum enim. Aenean consectetur imperdiet lectus, et condimentum tortor tempus at. Duis ac felis lectus. Proin feugiat nisl non diam semper, sit amet egestas purus egestas. Morbi eu ipsum nec odio tristique tincidunt ut sit amet nunc. Etiam nec lorem nisl. Duis sagittis diam velit, nec iaculis justo hendrerit a. Vivamus orci magna, consequat sodales risus nec, pharetra accumsan dui. Donec consequat magna eget libero iaculis, at egestas enim accumsan. Nullam volutpat auctor arcu, in blandit nulla malesuada eu. Etiam in leo varius, euismod ligula a, varius ligula. Sed viverra augue et urna tincidunt venenatis. In facilisis, felis sit amet cursus condimentum, est sapien tempor odio, eu pharetra nisi dolor vitae ipsum. Donec pellentesque ligula sed leo aliquet vestibulum. Donec sagittis libero odio, eu euismod nulla varius id.</p>
       <hr>


        <% LocalDateTime localTime = LocalDateTime.now(); %>
        <% out.println(localTime.toString()); %>

            </body>
</html>
