<%@ page import="java.time.LocalDateTime" %><%--
  Created by IntelliJ IDEA.
  User: jk222
  Date: 15/05/2018
  Time: 11:43 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>Text Example 2</title>
    </head>
    <body>

        <p>Dius imperdiet quam eget nunc sagittis, a ultricies massa sollicitudin. Proin laoreet sapien mi, sed ullamcorper ipsum sodales pretium. Praesent iaculis ipsum sodales lacus euismod molestie. Aliquam ac fringilla leo. Sed congue egestas turpis a lacinia. Aenean nec lobortis ligula. Pellentesque aliquet rhoncus est. Quisque interdum mi lorem, at placerat arcu tincidunt id. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Maecenas hendrerit nibh mi, ac consectetur mauris placerat nec.</p>

        <p>Donec tincidunt, odio nec commodo ultricies, tellus urna luctus turpis, in finibus magna metus at ligula. Nullam posuere nibh ut tincidunt luctus. In sit amet ex rhoncus, facilisis odio ac, convallis libero. Maecenas suscipit lectus ac diam interdum mattis. Nullam et imperdiet tellus. Curabitur in sapien blandit, gravida nunc non, facilisis dolor. Aliquam vel lacinia metus. Donec porttitor, magna vel luctus suscipit, diam massa interdum ante, vel eleifend lacus nunc a velit. Phasellus pretium vulputate magna, a dignissim metus rutrum nec. Integer sit amet urna tempor, tincidunt ex vel, maximus mauris. Donec ac turpis posuere, mattis quam non, malesuada metus. Pellentesque sollicitudin elit nec arcu malesuada congue. Phasellus bibendum iaculis ipsum, eu venenatis nunc tempus porttitor. Mauris ullamcorper, tellus non eleifend vestibulum, diam orci condimentum elit, a posuere justo dolor quis metus. Curabitur sit amet ipsum eu augue maximus dapibus. Ut posuere felis a urna rhoncus viverra eu vitae nisi.</p>
<hr>


        <% LocalDateTime localTime = LocalDateTime.now(); %>
        <% out.println(localTime.toString()); %>

    </body>
</html>
